import http.server
import socketserver
import subprocess
import threading

host = 'localhost'
port = 0
server = socketserver.TCPServer(
            (host, port), http.server.SimpleHTTPRequestHandler)

with server:
    host, port = server.server_address
    file = 'tod3d.html'

    server_thread = threading.Thread(target = server.serve_forever)
    server_thread.start()

    status, output = subprocess.getstatusoutput(
        f'firefox http://{host}:{port}/{file}')

    input('\nPress ENTER to exit web server\n\n')

    server.shutdown()
    server_thread.join()
