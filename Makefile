OBJS=main.o
CXX=g++
DEPS=sdl2 SDL2_gfx SDL2_image

CXXFLAGS=-g -O3 $(shell pkg-config --cflags $(DEPS)) -std=c++17
LDFLAGS=$(shell pkg-config --libs $(DEPS))

OBJ_NAME=tod3d

.PHONY: all clean

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(OBJ_NAME): $(OBJS)
	$(CXX) -o $@ $^ $(CXXFLAGS) $(LDFLAGS)

all: $(OBJ_NAME)

clean:
	rm -f $(OBJ_NAME) $(OBJS)
