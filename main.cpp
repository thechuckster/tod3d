#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <stdint.h>
#include <string>
#include <vector>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_stdinc.h>
#ifdef EMSCRIPTEN
#include <emscripten.h>
#endif

const int MAP_WIDTH=10, MAP_HEIGHT=9;
const int MAP[MAP_HEIGHT][MAP_WIDTH] ={
    {1,1,1,1,1,1,1,1,1,1},
    {2,0,0,0,0,0,0,0,0,4},
    {2,0,0,0,0,0,0,0,0,4},
    {2,0,0,0,0,0,0,0,0,4},
    {2,1,0,0,0,0,0,0,1,4},
    {2,0,0,0,0,0,0,0,0,4},
    {2,0,0,0,0,0,0,0,0,4},
    {2,0,0,0,0,0,0,0,0,4},
    {3,3,3,3,3,3,3,3,3,3}
};
const int SCREEN_WIDTH=640*2, SCREEN_HEIGHT=480*2;
const float CELL_SCALE = 0.5;   

const float startx=0.0, starty=0.0;
float playerx=startx, playery=starty;
float look_angle=0.0;
const float HFOV_DOUBLE=M_PI/3;
const float hfov=HFOV_DOUBLE/2; // half of the hFOV
const float VFOV_DOUBLE=M_PI/4/2;
const float vfov=VFOV_DOUBLE/2; // half of the vFOV
const float LOOK_DELTA=0.1;

const float SQRT2_2 = 0.70710678118;

float ALPHA_BUFFER[SCREEN_WIDTH] = {0.};

const std::vector<const char*> textures = {
    "assets/textures/brick.png",
    "assets/textures/demo067.png",
    "assets/textures/demo012.png",
    "assets/textures/demo008.png",
    "assets/textures/demo005.png"
};
std::vector<SDL_Surface*> images;

bool IntersectLine(bool debug, const float line1x, const float line1y, const float line2x, const float line2y, const float px, const float py, const float rayx, const float rayy, float& t, float& texture_t) {
    float dx=rayx, dy=rayy, ax=line1x, ay=line1y, bx=line2x, by=line2y, ox=px, oy=py;

    float orthox = -dy, orthoy = dx;
    float aToOx = ox - ax, aToOy = oy - ay;
    float aToBx = bx - ax, aToBy = by - ay;

    float denom = aToBx * orthox + aToBy * orthoy;
    if (abs(denom) < 1e-5) return false;

    float t1 = (aToBx * aToOy - aToOx * aToBy) / denom;
    float t2 = (aToOx * orthox + aToOy * orthoy) / denom;

    // printf("t1=%f; t2=%f\n", t1, t2);

    t = t1;
    texture_t = t2;
    return t2 >= 0 && t2 <= 1 && t1 >= 0;
}

int IntersectMap(bool debug, float px, float py, float rayx, float rayy, float& t, float& texture_t) {
    float line1x = 0, line1y = 0, line2x = 0, line2y = 0;

    float best_t = -1.0;
    float best_texture_t = -1.0;
    int best_color = 0;

    for (int i=0; i<MAP_WIDTH; ++i) {
        for (int j=0; j<MAP_HEIGHT; ++j) {
            if (MAP[j][i] == 0) continue;

            line1x = ((float)i-MAP_WIDTH/2.0-0.5)*CELL_SCALE;
            line1y = ((float)j-MAP_HEIGHT/2.0-0.5)*CELL_SCALE;
            line2x = ((float)i-MAP_WIDTH/2.0+0.5)*CELL_SCALE;
            line2y = ((float)j-MAP_HEIGHT/2.0-0.5)*CELL_SCALE;
            if (IntersectLine(debug, line1x, line1y, line2x, line2y, px, py, rayx, rayy, t, texture_t) && (best_t < 0 || t < best_t)) {
                best_t = t;
                best_color = MAP[j][i];
                best_texture_t = texture_t;
            }
            // printf("line: (%f, %f) to (%f, %f)\n", line1x, line1y, line2x, line2y);

            line1x = ((float)i-MAP_WIDTH/2.0-0.5)*CELL_SCALE;
            line1y = ((float)j-MAP_HEIGHT/2.0+0.5)*CELL_SCALE;
            line2x = ((float)i-MAP_WIDTH/2.0+0.5)*CELL_SCALE;
            line2y = ((float)j-MAP_HEIGHT/2.0+0.5)*CELL_SCALE;
            if (IntersectLine(debug, line1x, line1y, line2x, line2y, px, py, rayx, rayy, t, texture_t) && (best_t < 0 || t < best_t)) {
                best_t = t;
                best_color = MAP[j][i];
                best_texture_t = texture_t;
            }
            // printf("line: (%f, %f) to (%f, %f)\n", line1x, line1y, line2x, line2y);

            line1x = ((float)i-MAP_WIDTH/2.0-0.5)*CELL_SCALE;
            line1y = ((float)j-MAP_HEIGHT/2.0-0.5)*CELL_SCALE;
            line2x = ((float)i-MAP_WIDTH/2.0-0.5)*CELL_SCALE;
            line2y = ((float)j-MAP_HEIGHT/2.0+0.5)*CELL_SCALE;
            if (IntersectLine(debug, line1x, line1y, line2x, line2y, px, py, rayx, rayy, t, texture_t) && (best_t < 0 || t < best_t)) {
                best_t = t;
                best_color = MAP[j][i];
                best_texture_t = texture_t;
            }
            // printf("line: (%f, %f) to (%f, %f)\n", line1x, line1y, line2x, line2y);

            line1x = ((float)i-MAP_WIDTH/2.0+0.5)*CELL_SCALE;
            line1y = ((float)j-MAP_HEIGHT/2.0-0.5)*CELL_SCALE;
            line2x = ((float)i-MAP_WIDTH/2.0+0.5)*CELL_SCALE;
            line2y = ((float)j-MAP_HEIGHT/2.0+0.5)*CELL_SCALE;
            if (IntersectLine(debug, line1x, line1y, line2x, line2y, px, py, rayx, rayy, t, texture_t) && (best_t < 0 || t < best_t)) {
                best_t = t;
                best_color = MAP[j][i];
                best_texture_t = texture_t;
            }
            // printf("line: (%f, %f) to (%f, %f)\n", line1x, line1y, line2x, line2y);
        }
    }

    t = best_t;
    texture_t = best_texture_t;
    return best_color;
}

void PlotPixel(Uint32* pixels, int x, int y, Uint32 r, Uint32 g, Uint32 b) {
    if (x < 0 || y < 0 || x >= SCREEN_WIDTH || y >= SCREEN_HEIGHT) { return; }
    pixels[y*SCREEN_WIDTH+x] = b | g << 8 | r << 16;
}

void PlotLineLow(Uint32* pixels, Uint32 r, Uint32 g, Uint32 b, int x0, int y0, int x1, int y1) {
    int dx = x1 - x0;
    int dy = y1 - y0;
    int yi = 1;
    if (dy < 0) {
        yi = -1;
        dy = -dy;
    }
    int D = (2 * dy) - dx;
    int y = y0;

    for (int x = x0; x <= x1; x++) {
        PlotPixel(pixels, x, y, r, g, b);
        if (D > 0) {
            y = y + yi;
            D = D + (2 * (dy - dx));
        } else {
            D = D + 2*dy;
        }
    }
}

void PlotLineHigh(Uint32* pixels, Uint32 r, Uint32 g, Uint32 b, int x0, int y0, int x1, int y1) {
    int dx = x1 - x0;
    int dy = y1 - y0;
    int xi = 1;
    if (dx < 0) {
        xi = -1;
        dx = -dx;       
    }
    int D = (2 * dx) - dy;
    int x = x0;

    for (int y = y0; y <= y1; y++) {
        PlotPixel(pixels, x, y, r, g, b);
        if (D > 0) {
            x = x + xi;
            D = D + (2 * (dx - dy));
        } else {
            D = D + 2*dx;
        }
    }
}

void DrawLine(Uint32* pixels, Uint32 r, Uint32 g, Uint32 b, int x0, int y0, int x1, int y1) {
    if (abs(y1 - y0) < abs(x1 - x0)) {
        if (x0 > x1) {
            PlotLineLow(pixels, r, g, b, x1, y1, x0, y0);
        } else {
            PlotLineLow(pixels, r, g, b, x0, y0, x1, y1);
        }       
    } else {
        if (y0 > y1) {
            PlotLineHigh(pixels, r, g, b, x1, y1, x0, y0);
        } else {
            PlotLineHigh(pixels, r, g, b, x0, y0, x1, y1);
        }       
    }
}

void DrawDebugLine(Uint32* pixels, Uint32 r, Uint32 g, Uint32 b, float x0, float y0, float x1, float y1) {
    const float DEBUG_SCALE = 25.0;
    const float xoffset=(MAP_WIDTH/2.0+1.0)*CELL_SCALE*DEBUG_SCALE, yoffset=(MAP_HEIGHT/2.0+1.0)*CELL_SCALE*DEBUG_SCALE;
    DrawLine(pixels, r, g, b, x0*DEBUG_SCALE + xoffset, y0*DEBUG_SCALE + yoffset, x1*DEBUG_SCALE + xoffset, y1*DEBUG_SCALE + yoffset);
}

void DrawMap(bool debug, Uint32* pixels, const float playerx, const float playery, float lookx, float looky) {
    // float neglookhatx=-looky, neglookhaty=lookx;
    // float poslookhatx=looky, poslookhaty=lookx;
    for (int x=0; x<SCREEN_WIDTH; x++) {        
        // shoot a ray from (playerx, playery) to (playerx+lookx, playery+looky)

        // normalized_x \in [-1.0, 1.0]
        float alpha = ALPHA_BUFFER[x];
        // float rayx = lookx+poslookhatx*tanf(alpha), rayy = looky+poslookhaty*tanf(alpha);
        float rayx = lookx*cosf(alpha)-looky*sinf(alpha);
        float rayy = lookx*sinf(alpha)+looky*cosf(alpha);

        if (!debug) {
            // figure out what cell we are at
            float t = 0;
            float texture_u = 0;
            int cell = IntersectMap(x == SCREEN_WIDTH / 2, playerx, playery, rayx, rayy, t, texture_u);
            t *= cos(alpha); // undo fisheye
            SDL_Surface* image = nullptr;
            int w = 0, h = 0;
            int tx = 0;
            if (cell-1 >= 0 && cell-1 < images.size()) {
                image = images[cell-1];
                w = image->w; h = image->h;
                tx = std::max(0, std::min((int)(w*texture_u), w));                
            }
            // int cell = 1;
            // float height = 5; // this should be proportional to the distance, right?!        
            // then plot the appropriate color from (-height, height)

            int ymin=-SCREEN_HEIGHT/2 / t;
            int ymax=SCREEN_HEIGHT/2 / t;

            int ystart = std::max(0, std::min(ymin+SCREEN_HEIGHT/2, SCREEN_HEIGHT));
            int yend = std::max(0, std::min(ymax+SCREEN_HEIGHT/2, SCREEN_HEIGHT));
            for (int y=0; y<SCREEN_HEIGHT; y++) {
                float normalized_y = ((float)y - SCREEN_HEIGHT/2.0)/(SCREEN_HEIGHT/2.0);
                float beta = normalized_y*vfov;            

                Uint32 r = 0, g = 0, b = 0;
                if (y < ystart) {
                    r = 255; g = 255; b = 0; 
                } else if (y > yend) {
                    r = 255; g = 0; b = 255; 
                } else if (cell != 0) {
                    const float t_min = 1.0;
                    const float t_max = 10.0;
                    const float lerp_min = 0.1;
                    const float lerp_max = 1.0;
                    float lerp_factor = cell != 0 ? 1.0 - std::min(lerp_max, std::max(lerp_min, std::max((float)0.0, std::min((float)1.0, (t - t_min) / (t_max - t_min))))) : 1.0;
                    // if (lerp_factor < 1.0) printf("lerp_factor=%f\n", lerp_factor);
                    float texture_v = yend != ystart ? ((float)y - (float)ymin - SCREEN_HEIGHT/2) / ((float)ymax - (float)ymin) : 0.0;

                    // look up pixel (texture_u, texture_v)
                    float texture_sample_r = 1., texture_sample_g = 1., texture_sample_b = 1.;
                    int ty = std::max(0, std::min((int)(h*texture_v), h));
                    if (image != nullptr) {
                        uint8_t* px = (uint8_t*)image->pixels;
                        texture_sample_r = px[(w*ty+tx)*image->format->BytesPerPixel+0];
                        texture_sample_g = px[(w*ty+tx)*image->format->BytesPerPixel+1];
                        texture_sample_b = px[(w*ty+tx)*image->format->BytesPerPixel+2];
                    }

                    r = (Uint32)(/* (float)r * */ lerp_factor * texture_sample_r);
                    g = (Uint32)(/* (float)g * */ lerp_factor * texture_sample_g);
                    b = (Uint32)(/* (float)b * */ lerp_factor * texture_sample_b);
                }
                PlotPixel(pixels, x, y, r, g, b);
            }
        }

        if (debug) {
            /* // debug rays
            if (x == 0) {
                printf("MIN ray: (%f, %f) %f\n", rayx, rayy, alpha);
            } else if (x == SCREEN_WIDTH) {
                printf("MAX ray: (%f, %f) %f\n", rayx, rayy, alpha);
            } */
            if (x == 0 || x == (SCREEN_WIDTH-1)) DrawDebugLine(pixels, 255, 0, 0, playerx, playery, playerx + rayx*5, playery + rayy*5);              
        }
    }

    if (debug) {
        // printf("player: (%f, %f)\n", playerx, playery);
        // printf("look: (%f, %f)\n", lookx, looky);
    }
}

void DrawDebugMap(Uint32* pixels, const float playerx, const float playery, float lookx, float looky) {
    // draw debug map
    // DrawDebugLine(pixels, 255, 0, 0, 0, 0, 25, 25);
    for (int i=0; i<MAP_WIDTH; ++i) {
        for (int j=0; j<MAP_HEIGHT; ++j) {
            Uint32 r=0, g=0, b=0;
            int cell=MAP[j][i];
            if (cell == 0) {
                continue;
            } else if (cell == 1) {
                r = 255; g = 0; b = 0;
            } else if (cell == 2) {
                r = 0; g = 255; b = 0;
            } else if (cell == 3) {
                r = 0; g = 0; b = 255;
            } else if (cell == 4) {
                r = 255; g = 255; b = 0;
            }

            float line1x, line1y, line2x, line2y;

            line1x = ((float)i-MAP_WIDTH/2.0-0.5)*CELL_SCALE;
            line1y = ((float)j-MAP_HEIGHT/2.0-0.5)*CELL_SCALE;
            line2x = ((float)i-MAP_WIDTH/2.0+0.5)*CELL_SCALE;
            line2y = ((float)j-MAP_HEIGHT/2.0-0.5)*CELL_SCALE;
            DrawDebugLine(pixels, r, g, b, line1x, line1y, line2x, line2y);
            // printf("line: (%f, %f) to (%f, %f)\n", line1x, line1y, line2x, line2y);

            line1x = ((float)i-MAP_WIDTH/2.0-0.5)*CELL_SCALE;
            line1y = ((float)j-MAP_HEIGHT/2.0+0.5)*CELL_SCALE;
            line2x = ((float)i-MAP_WIDTH/2.0+0.5)*CELL_SCALE;
            line2y = ((float)j-MAP_HEIGHT/2.0+0.5)*CELL_SCALE;
            DrawDebugLine(pixels, r, g, b, line1x, line1y, line2x, line2y);
            // printf("line: (%f, %f) to (%f, %f)\n", line1x, line1y, line2x, line2y);

            line1x = ((float)i-MAP_WIDTH/2.0-0.5)*CELL_SCALE;
            line1y = ((float)j-MAP_HEIGHT/2.0-0.5)*CELL_SCALE;
            line2x = ((float)i-MAP_WIDTH/2.0-0.5)*CELL_SCALE;
            line2y = ((float)j-MAP_HEIGHT/2.0+0.5)*CELL_SCALE;
            DrawDebugLine(pixels, r, g, b, line1x, line1y, line2x, line2y);
            // printf("line: (%f, %f) to (%f, %f)\n", line1x, line1y, line2x, line2y);

            line1x = ((float)i-MAP_WIDTH/2.0+0.5)*CELL_SCALE;
            line1y = ((float)j-MAP_HEIGHT/2.0-0.5)*CELL_SCALE;
            line2x = ((float)i-MAP_WIDTH/2.0+0.5)*CELL_SCALE;
            line2y = ((float)j-MAP_HEIGHT/2.0+0.5)*CELL_SCALE;
            DrawDebugLine(pixels, r, g, b, line1x, line1y, line2x, line2y);
            // printf("line: (%f, %f) to (%f, %f)\n", line1x, line1y, line2x, line2y);
        }
    }
    // DrawDebugLine(pixels, 255, 0, 0, SCREEN_WIDTH/2 + playerx*CELL_SCALE, SCREEN_HEIGHT/2 + playery*CELL_SCALE, SCREEN_WIDTH/2 + playerx*CELL_SCALE, SCREEN_HEIGHT/2 + playery*CELL_SCALE);

    // draw player and vector
    DrawMap(true, pixels, playerx, playery, lookx, looky);
}

struct Context {
    unsigned int lastTime = 0;
    bool w=false, a=false, s=false, d=false;
    bool quit = false;
    SDL_Window* window=nullptr;
    SDL_Renderer* renderer=nullptr;
    SDL_Texture* texture=nullptr;
    Uint32* pixels=nullptr;
};

void LoopHandler(void* arg) {
    struct Context* ctx = (struct Context*)arg;

    Uint32 currentTime = SDL_GetTicks();
    float dt = (currentTime - ctx->lastTime) / 100.0;
    ctx->lastTime = currentTime;

    SDL_Event event;
    while (SDL_PollEvent(&event) != 0) {
        if (event.type == SDL_KEYDOWN) {
            switch (event.key.keysym.sym) {
                case SDLK_ESCAPE:
                    ctx->quit = true;
                    break;
                default:
                    ;
            }
        } else if (event.type == SDL_QUIT) {
            ctx->quit = true;
        }
    }

    const Uint8* currentKeyStates = SDL_GetKeyboardState(NULL);

    const float LOOK_SPEED = 1.5; // / 2.0;
    if (currentKeyStates[SDL_SCANCODE_D]) { look_angle -= LOOK_DELTA * dt * LOOK_SPEED; }
    if (currentKeyStates[SDL_SCANCODE_A]) { look_angle += LOOK_DELTA * dt * LOOK_SPEED; }

    float lookx=sinf(look_angle), looky=cosf(look_angle);

    const float MOVE_SPEED = 1.0 / 5.0;
    float deltax = 0., deltay = 0.;
    if (currentKeyStates[SDL_SCANCODE_W]) { deltax += lookx * dt * MOVE_SPEED; deltay += looky * dt * MOVE_SPEED; }
    if (currentKeyStates[SDL_SCANCODE_S]) { deltax -= lookx * dt * MOVE_SPEED; deltay -= looky * dt * MOVE_SPEED; }

    float targetx = playerx + deltax, targety = playery + deltay;
    int num_collisions = 0;
    const float BUFFER = 1e-3;
    // printf("--\n");
    for (int i=0; i<MAP_WIDTH; ++i) {
        for (int j=0; j<MAP_HEIGHT; ++j) {
            if (MAP[j][i] == 0) { continue; }
            float xmin = ((float)i-MAP_WIDTH/2.0-0.5)*CELL_SCALE - BUFFER;
            float ymin = ((float)j-MAP_HEIGHT/2.0-0.5)*CELL_SCALE - BUFFER;
            float xmax = ((float)i-MAP_WIDTH/2.0+0.5)*CELL_SCALE + BUFFER;
            float ymax = ((float)j-MAP_HEIGHT/2.0+0.5)*CELL_SCALE + BUFFER;
            /* bool decreased_xmin=(i > 0 && MAP[j][i-1] != 0), decreased_ymin=(j > 0 && MAP[j-1][i] != 0), increased_xmax=(i+1 < MAP_WIDTH && MAP[j][i+1] != 0), increased_ymax=(j+1 < MAP_HEIGHT && MAP[j+1][i] != 0);
            int num_x = decreased_xmin + increased_xmax;
            int num_y = decreased_ymin + increased_ymax;
            if (num_x * num_y == 0) {
                if (decreased_xmin) {
                    xmin = ((float)(i-1)-MAP_WIDTH/2.0-0.5)*CELL_SCALE - BUFFER;
                    decreased_xmin=true;
                }
                if (decreased_ymin) {
                    ymin = ((float)(j-1)-MAP_HEIGHT/2.0-0.5)*CELL_SCALE - BUFFER;
                    decreased_ymin=true;
                }
                if (increased_xmax) {
                    xmax = ((float)(i+1)-MAP_WIDTH/2.0+0.5)*CELL_SCALE + BUFFER;
                    increased_xmax=true;
                }
                if (increased_ymax) {
                    ymax = ((float)(j+1)-MAP_HEIGHT/2.0+0.5)*CELL_SCALE + BUFFER;
                    increased_ymax=true;
                }                
            } */
            if (targetx >= xmin && targetx <= xmax && targety >= ymin && targety <= ymax) {
                num_collisions += 1;
                /* printf("increment: num_collisions=%d\n", num_collisions);
                if (decreased_xmin) { printf("decreased_xmin\n"); }
                if (decreased_ymin) { printf("decreased_ymin\n"); }
                if (increased_xmax) { printf("increased_xmax\n"); }
                if (increased_ymax) { printf("increased_ymax\n"); } */
                // printf("(i=%d,j=%d)=%d (i-1=%d,j=%d)=%d (i+1=%d,j=%d)=%d\n", i,j, MAP[j][i], i-1, j, i > 0 ? MAP[j][i-1] : -1, i+1, j, i+1 < MAP_WIDTH ? MAP[j][i+1] : -1);

                // calculate the normal
                float normalx = 0., normaly = 0.;
                if (playerx >= xmin && playerx <= xmax && playery <= ymin) {
                    normalx = 0.; normaly = -1.;
                    // printf("DOWN NORMAL\n");
                } else if (playerx >= xmin && playerx <= xmax && playery >= ymax) {
                    normalx = 0.; normaly = 1.;
                    // printf("UP NORMAL\n");
                } else if (playery >= ymin && playery <= ymax && playerx <= xmin) {
                    normalx = -1.; normaly = 0.;
                    // printf("LEFT NORMAL\n");
                } else if (playery >= ymin && playery <= ymax && playerx >= xmax) {
                    normalx = 1.; normaly = 0.;
                    // printf("RIGHT NORMAL\n");

                /* } else if (playerx <= xmin && playery <= ymin) {
                    normalx = -SQRT2_2; normaly = -SQRT2_2;
                } else if (playerx <= xmin && playery >= ymax) {
                    normalx = -SQRT2_2; normaly = SQRT2_2;
                } else if (playery >= ymin && playery <= ymax && playerx <= xmin) {
                    normalx = -SQRT2_2; normaly = SQRT2_2;
                } else if (playery >= ymin && playery <= ymax && playerx >= xmax) {
                    normalx = SQRT2_2; normaly = -SQRT2_2; */

                } /* else {
                    printf("CORNER CASE NORMAL (x >= xmin: %d); (x <= xmax: %d); (y >= ymin: %d); (y <= ymax: %d)\n", playerx >= xmax, playerx <= xmax, playery >= ymin, playery <= ymax);
                    // no point in going any further if we've already stopped the player's motion
                    // deltax = 0; deltay = 0;
                    // break;
                } */

                // TODO: ONLY ALLOW CORNER CASES WITH TRUE CORNERS

                // printf("COLLIDE WITH (xmin=%f, ymin=%f) (xmax=%f, ymax=%f)\n", xmin, ymin, xmax, ymax);

                const float dot = deltax * normalx + deltay * normaly;
                deltax -= normalx * dot;
                deltay -= normaly * dot;

                /* if (playerx >= xmax && playerx + deltax <= xmax) {
                    deltax = xmax - playerx;
                    // printf("CLAMP XMAX\n");
                }
                if (playerx <= xmin && playerx + deltax >= xmin) {
                    deltax = xmin - playerx;
                    // printf("CLAMP XMIN\n");
                }
                if (playery >= ymax && playery + deltay <= ymax) {
                    deltay = ymax - playery;
                    // printf("CLAMP YMAX\n");
                }
                if (playery <= ymin && playery + deltay >= ymin) {
                    deltay = ymin - playery;
                    // printf("CLAMP YMIN\n");
                } */

                targetx = playerx + deltax;
                targety = playery + deltay;

                break;
            }
        }
    }
    playerx += deltax; playery += deltay;
    // if (num_collisions != 0) { printf("num_collisions=%d\n", num_collisions); }

    SDL_SetRenderDrawColor(ctx->renderer, 0, 0, 0, 255);
    SDL_RenderClear(ctx->renderer);

    memset(ctx->pixels, 0, SCREEN_WIDTH * SCREEN_HEIGHT * sizeof(Uint32));

    DrawMap(false, ctx->pixels, playerx, playery, lookx, looky);
    DrawDebugMap(ctx->pixels, playerx, playery, lookx, looky);
    SDL_UpdateTexture(ctx->texture, NULL, ctx->pixels, SCREEN_WIDTH * sizeof(Uint32));

    SDL_RenderCopy(ctx->renderer, ctx->texture, NULL, NULL);
    SDL_RenderPresent(ctx->renderer);
}

int main(int argc, char ** argv) {
    struct Context ctx;    

    SDL_Init(SDL_INIT_VIDEO);

    ctx.window = SDL_CreateWindow("Tales of Dagur 3D", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, 0);

    ctx.renderer = SDL_CreateRenderer(ctx.window, -1, 0);
    ctx.texture = SDL_CreateTexture(ctx.renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, SCREEN_WIDTH, SCREEN_HEIGHT);

    ctx.pixels = new Uint32[SCREEN_WIDTH * SCREEN_HEIGHT];

    // how to unload and free?!
    for (const char* filename: textures) {
        SDL_Surface* image = IMG_Load(filename);
        printf("IMG_Load: %s\n", IMG_GetError());
        printf("w=%d, h=%d, pitch=%d\n", image->w, image->h, image->pitch);
        printf("Format=%d, BitsPerPixel=%d, BytesPerPixel=%d, Rmask=%d, Gmask=%d, Bmask=%d, Amask=%d\n",
            image->format->format,
            image->format->BitsPerPixel,
            image->format->BytesPerPixel,
            image->format->Rmask,
            image->format->Gmask,
            image->format->Bmask,
            image->format->Amask);
        images.push_back(image);
    }

    // precompute alpha angles
    for (int x=0; x<SCREEN_WIDTH; x++) {
        float normalized_x = ((float)x - SCREEN_WIDTH/2.0)/(SCREEN_WIDTH/2.0);
        ALPHA_BUFFER[x] = normalized_x*hfov;        
    }

    ctx.lastTime = SDL_GetTicks();
    ctx.w=false; ctx.a=false; ctx.s=false; ctx.d=false;
#ifdef EMSCRIPTEN
    emscripten_set_main_loop_arg(LoopHandler, &ctx, -1, 1);    
#else
    while (!ctx.quit) {
        LoopHandler(&ctx);
    }
#endif

    delete[] ctx.pixels;
    SDL_DestroyTexture(ctx.texture);
    SDL_DestroyRenderer(ctx.renderer);

    SDL_DestroyWindow(ctx.window);
    SDL_Quit();

    return 0;
}
